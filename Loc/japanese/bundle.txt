﻿BUNDLE_NAME_ENUM_001	Commencement Bundle
BUNDLE_DESC_ENUM_001	★Only available for 7 days!★<br>An exclusive bundle to help you kick-start your adventure!<br>Use [Start Dash] Featured 5★ 50-Soul Shard Selector to select 50 5★ Soul Shards of your choice. (Hazel / Lupinus / Reimei / Vettel / Elizabeth / Fung Liu / Shayna / Shenmei / Aruba / Magnus / Teona / Flamel / Yomi / Ryle / Spica / Suzuka / Anastasia / Aswald / Rosa / Zahar)
BUNDLE_NAME_ENUM_002	Apprentice Bundle
BUNDLE_DESC_ENUM_002	A decent set of essentials for any budding Apprentices!
BUNDLE_NAME_ENUM_003	Journeyman Bundle
BUNDLE_DESC_ENUM_003	A generous box of supplies for any seasoned Journeyman!
BUNDLE_NAME_ENUM_004	Ouroboros's Hoard
BUNDLE_DESC_ENUM_004	A valuable set of items and Gems
BUNDLE_NAME_ENUM_005	Master Bundle
BUNDLE_DESC_ENUM_005	A incredible Crate of treasures for any renowned master!
BUNDLE_NAME_ENUM_006	Ouroboros's Crate
BUNDLE_DESC_ENUM_006	A valuable set of items and Gems
BUNDLE_NAME_ENUM_007	Ouroboros's Keepsake
BUNDLE_DESC_ENUM_007	A valuable set of items and Gems
BUNDLE_NAME_ENUM_008	Ouroboros's Cache
BUNDLE_DESC_ENUM_008	A valuable set of items and Gems
BUNDLE_NAME_ENUM_009	Ouroboros's Chest
BUNDLE_DESC_ENUM_009	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0010	Ouroboros's Vault
BUNDLE_DESC_ENUM_0010	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0011	Ouroboros's Treasury
BUNDLE_DESC_ENUM_0011	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0012	Ouroboros's Trinket
BUNDLE_DESC_ENUM_0012	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0013	Ouroboros's Pendant
BUNDLE_DESC_ENUM_0013	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0013_01	Black Friday Bundle
BUNDLE_DESC_ENUM_0013_01	★ Black Friday Special Offer! ★<br>An exclusive bundle to unleash the true potential of your units!
BUNDLE_NAME_ENUM_009_01	Black Friday Mega Bundle
BUNDLE_DESC_ENUM_009_01	★ Black Friday Special Offer!★<br>An exclusive bundle to make your strongest units even stronger!
BUNDLE_NAME_ENUM_0013_02	Winter Holiday Bundle
BUNDLE_DESC_ENUM_0013_02	★ Winter Holiday Exclusive Offer!★<br>A bundle of goodies to delight you this holiday season.
BUNDLE_NAME_ENUM_009_02	Winter Holiday Mega Bundle
BUNDLE_DESC_ENUM_009_02	★ Winter Holiday Exclusive Offer!★<br>A sack of presents to celebrate this winter festive season.
BUNDLE_NAME_ENUM_0012_01	Amazon Mystic Pouch
BUNDLE_DESC_ENUM_0012_01	★ Amazon Exclusive Offer!★<br>A mysterious pouch engraved with a smile on one side.
BUNDLE_NAME_ENUM_007_01	Amazon Mystic Chest
BUNDLE_DESC_ENUM_007_01	★ Amazon Exclusive Offer!★<br>A mysterious chest marked only by a smile on the lid.
BUNDLE_NAME_ENUM_0014_01	Cupid's Memento
BUNDLE_DESC_ENUM_0014_01	★ Valentine's Day Exclusive Offer!★<br>A delightful bit of Valentine's Day to warm your heart.
BUNDLE_NAME_ENUM_008_01	Cupid's Bouquet 
BUNDLE_DESC_ENUM_008_01	★ Valentine's Day Exclusive Offer!★<br>A beautiful bouquet made by Cupid for your perfect Valentine's Day.
BUNDLE_NAME_ENUM_009_03F	Valentine's Sweetheart Bundle
BUNDLE_DESC_ENUM_009_03F	★ Valentine's Day Exclusive Offer!★<br>A special date with your sweetheart for your sweetest Valentine's Day.
BUNDLE_NAME_ENUM_009_03M	Valentine's Heartthrob Bundle
BUNDLE_DESC_ENUM_009_03M	★ Valentine's Day Exclusive Offer!★<br>A special date with your heartthrob for your sweetest Valentine's Day.
BUNDLE_NAME_ENUM_0015	Ouroboros's Reunion Bundle
BUNDLE_DESC_ENUM_0015	★ Welcome back!★<br>A surprise hoard of goodies that Ouroborus kept for you when you were away. Only available for 7 days!
BUNDLE_NAME_ENUM_0014_02	2 Million Celebration Bundle
BUNDLE_DESC_ENUM_0014_02	A special offer to commemorate our 2 Million Celebration! Thank you for your continued support!
BUNDLE_NAME_ENUM_008_02	2 Million Celebration Deluxe Bundle
BUNDLE_DESC_ENUM_008_02	A special offer to commemorate our 2 Million Celebration! Thank you for your continued support!
BUNDLE_NAME_ENUM_001a	First Purchase Flash Bundle
BUNDLE_DESC_ENUM_001a	★ Exclusive offer!★<br>Power comes to those who seize the opportunity when it presents itself.
BUNDLE_DESC_ENUM_GEM_001	An extremely valuable set with great amount of Gems.
BUNDLE_NAME_ENUM_009_04	Spring Festival Bundle
BUNDLE_DESC_ENUM_009_04	★Spring Festival Special Offer!★<br>An exclusive bundle to celebrate this festive season.
BUNDLE_NAME_ENUM_0016	Ouroboros's Earrings
BUNDLE_DESC_ENUM_0016	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0017	Ouroboros's Ring
BUNDLE_DESC_ENUM_0017	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0015a	Ouroboros's Bangle
BUNDLE_DESC_ENUM_0015a	A valuable set of items and Gems
BUNDLE_NAME_ENUM_003a	Ouroboros's Stash
BUNDLE_DESC_ENUM_003a	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0018	Ouroboros's Coffer
BUNDLE_DESC_ENUM_0018	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0019	Ouroboros's Bracelet
BUNDLE_DESC_ENUM_0019	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0020	Ouroboros's Tiara
BUNDLE_DESC_ENUM_0020	A valuable set of items and Gems
BUNDLE_DESC_ENUM_008_03	An exclusive bundle to unleash the true potential of your units!
BUNDLE_DESC_ENUM_009_05	An exclusive bundle to make your strongest units even stronger!
BUNDLE_NAME_ENUM_008_04	5 Million Celebration Bundle
BUNDLE_DESC_ENUM_008_04	A special offer to commemorate our 5 Million Celebration! Thank you for your continued support!
BUNDLE_NAME_ENUM_009_06	5 Million Celebration Deluxe Bundle
BUNDLE_DESC_ENUM_009_06	A special offer to commemorate our 5 Million Celebration! Thank you for your continued support!
BUNDLE_NAME_ENUM_0011_01	5 Million Celebration Mega Bundle
BUNDLE_DESC_ENUM_0011_01	A special offer to commemorate our 5 Million Celebration! Thank you for your continued support!
BUNDLE_NAME_ENUM_008_05	Global v2.0 Celebration Bundle
BUNDLE_DESC_ENUM_008_05	A special offer to commemorate our Global Version 2.0 Celebration Bundle.
BUNDLE_NAME_ENUM_009_07	Global v2.0 Celebration Deluxe Bundle
BUNDLE_DESC_ENUM_009_07	A special offer to commemorate our Global Version 2.0 Celebration Bundle.<br>Use 4★ Gear Selector to obtain a 4★ Gear of your choice, excluding Golden Jade Tarot, Serenity Chakram, Collaboration Gears and Sacred Stone Gears.
BUNDLE_NAME_ENUM_007_02	Ouroboros's Keepsake
BUNDLE_DESC_ENUM_007_02	An exclusive bundle to unleash the true potential of your units!<br>Use 5★ 25-Soul Shard Selector to select 25 5★ Soul Shards of your choice, excluding Setsuna Soul Shards, Sophia Soul Shards, Collaboration Soul Shards and Sacred Stone Soul Shards.
BUNDLE_NAME_ENUM_008_06	Ouroboros's Cache
BUNDLE_DESC_ENUM_008_06	An exclusive bundle to unleash the true potential of your units!<br>Use 5★ 50-Soul Shard Selector to select 50 5★ Soul Shards of your choice, excluding Setsuna Soul Shards, Sophia Soul Shards, Collaboration Soul Shards and Sacred Stone Soul Shards.
BUNDLE_NAME_ENUM_009_08	Ouroboros's Chest
BUNDLE_DESC_ENUM_009_08	An exclusive bundle to unleash the true potential of your units!<br>Use 5★ 50-Soul Shard Selector to select 50 5★ Soul Shards of your choice, excluding Setsuna Soul Shards, Sophia Soul Shards, Collaboration Soul Shards and Sacred Stone Soul Shards.
BUNDLE_NAME_ENUM_005a	Ouroboros's Jewellery
BUNDLE_DESC_ENUM_005a	A valuable set of items and Gems
BUNDLE_DESC_ENUM_GEM_002	An extremely valuable set with great amount of Gems. Extra freebies included!
BUNDLE_NAME_ENUM_0017_01	Amazon Prime Bundle
BUNDLE_DESC_ENUM_0017_01	★Amazon Prime Exclusive Offer!★<br>An extremely valuable set with great amount of Gems.<br>Discount is only applicable for Amazon Prime Members.        
BUNDLE_NAME_ENUM_0013_03	Amazon Prime Deluxe Bundle
BUNDLE_DESC_ENUM_0013_03	★Amazon Prime Exclusive Offer!★<br>An extremely valuable set with great amount of Gems.<br>Discount is only applicable for Amazon Prime Members.        
BUNDLE_NAME_ENUM_WEEKLY_01	Sapphire Bundle
BUNDLE_DESC_ENUM_WEEKLY_01	A decent set of essentials.
BUNDLE_NAME_ENUM_WEEKLY_02	Ruby Bundle
BUNDLE_DESC_ENUM_WEEKLY_02	A generous box of supplies.
BUNDLE_NAME_ENUM_WEEKLY_03	Emerald Bundle
BUNDLE_DESC_ENUM_WEEKLY_03	An incredible crate of treasures.
BUNDLE_NAME_ENUM_SUMMER2018_01	Summer Sports Meet Bundle
BUNDLE_DESC_ENUM_SUMMER2018_01	★Summer Sports Meet Special Offer!★ Equip your units and become a winner!
BUNDLE_NAME_ENUM_SUMMER2018_02	Summer Sports Meet Deluxe Bundle
BUNDLE_DESC_ENUM_SUMMER2018_02	★Summer Sports Meet Special Offer!★ Equip your units and become a winner!<br>Use [Summer Sports Meet] 4★ Gear Selector to obtain a 4★ Gear with MOVE/AGI boost of your choice. (Some 4★ Gears are not available in this selector)
BUNDLE_NAME_ENUM_SUMMER2018_03	Summer Sports Meet Mega Bundle
BUNDLE_DESC_ENUM_SUMMER2018_03	★Summer Sports Meet Special Offer!★ Equip your units and become a winner!<br>Use [Summer Sports Meet] 4★ Gear Selector to obtain a piece of 4★ gear with the MOVE/AGI boost of your choice. (Some 4★ gear is not available in this selector)
BUNDLE_NAME_ENUM_0021	Ouroboros's Brooch
BUNDLE_DESC_ENUM_0021	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0014	Ouroboros's Locket
BUNDLE_DESC_ENUM_0014	A valuable set of items and Gems
BUNDLE_NAME_ENUM_0014_03	Sapphire Bundle
BUNDLE_DESC_ENUM_0014_03	A decent set of essentials.<br>Use [Starter] 5★ 25-Soul Shard Selector to select 25 5★ Soul Shards of your choice. (Anastasia/Magnus/Vettel/Yomi/Shenmei/Ryle)
BUNDLE_NAME_ENUM_007_03	Halloween Candy Basket
BUNDLE_DESC_ENUM_007_03	★Halloween Exclusive Offer!★<br>A spook-tacular candy basket to celebrate this Halloween season.
BUNDLE_NAME_ENUM_008_07	Trick-or-Treat Bag
BUNDLE_DESC_ENUM_008_07	★Halloween Exclusive Offer!★<br>A spook-tacular Trick-or-Treat Bag to celebrate this Halloween season.<br>Use Trick-or-Treat 10-Summon Ticket to summon 10 Spooky Halloween Surprises, with one guaranteed 15 Featured 5★ Unit Soul Shards (Uzuma/Leoniaz/Ravina/Basheeny/Roxanne/Noah/Zofia)
BUNDLE_NAME_ENUM_0014_04	[7 Million Downloads Worldwide!] Bundle
BUNDLE_DESC_ENUM_0014_04	A special offer to commemorate our 7 Million Downloads Worldwide Celebration! Thank you for your continued support!
BUNDLE_NAME_ENUM_009_09	[7 Million Downloads Worldwide!] Deluxe Bundle
BUNDLE_DESC_ENUM_009_09	A special offer to commemorate our 7 Million Downloads Worldwide Celebration! Thank you for your continued support!
SUB_NAME_ENUM_1STANNI_01	[1st Anniversary] Subscription
SUB_DESC_ENUM_1STANNI_01	★1st Anniversary Exclusive Offer!★<br> For a limited time, purchase this subscription and get 200 Free Gems daily for 30 days.<br>Gems will be sent to Gifts.
BUNDLE_NAME_ENUM_1STPURCHASE_01	First Purchase Flash Bundle
BUNDLE_DESC_ENUM_1STPURCHASE_01	★Exclusive offer!★<br>Power comes to those who seize the opportunity when it presents itself.<br>Use [Start Dash] Featured 5★ 50-Soul Shard Selector to select 50 5★ Soul Shards of your choice. (Hazel / Lupinus / Reimei / Vettel / Elizabeth / Fung Liu / Shayna / Shenmei / Aruba / Magnus / Teona / Flamel / Yomi / Ryle / Spica / Suzuka / Anastasia / Aswald / Rosa / Zahar)
BUNDLE_NAME_ENUM_WEEKLY_03_01	Emerald Bundle
BUNDLE_DESC_ENUM_WEEKLY_03_01	An incredible crate of treasures.<br>Use 5★ Unit with 25 Shards Summon Ticket to summon a 5★ Unit and 25 Shards of that Unit.
BUNDLE_NAME_ENUM_008_08	Black Friday Deluxe Bundle
BUNDLE_DESC_ENUM_008_08	★Black Friday Special Offer!★<br>An exclusive bundle to make your strongest units even stronger!<br>Use 5★ Gear Summon Ticket to summon a 5★ version of a 4★ gear item.
BUNDLE_NAME_ENUM_009_10	Black Friday Mega Bundle
BUNDLE_DESC_ENUM_009_10	★Black Friday Special Offer!★<br>An exclusive bundle to make your strongest units even stronger!<br>Use 5★ Gear Summon Ticket to summon a 5★ version of a 4★ gear item.
BUNDLE_NAME_ENUM_008_09	Winter Remedial Bundle
BUNDLE_DESC_ENUM_008_09	★Winter Holiday Exclusive Offer!★<br>Have a cozy private lesson with Minerva this Winter season.<br>Use Minerva's Delight 10-Summon Ticket to Summon 10 Winter Holiday Surprises, with one guaranteed 15 Featured 5★ Unit Soul Shards ( Birgitta / Acht / Vier / Bashenny / Minerva / Kilma / Itsuki )
BUNDLE_NAME_ENUM_006_01	Winter Holiday Mega Bundle
BUNDLE_DESC_ENUM_006_01	★Winter Holiday Exclusive Offer!★<br>A sack of presents to celebrate this winter festive season.<br>Use 5★ Gear Summon Ticket to summon a 5★ version of a 4★ gear item.
